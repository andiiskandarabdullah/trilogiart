@extends('web.partials.master')

@section('title') Trilogi Project @endsection
@section('styles')
    <style>
        .card {
            border: none;
            border-radius: 10px
        }

        .percent {
            padding: 5px;
            background-color: red;
            border-radius: 5px;
            color: #fff;
            font-size: 14px;
            height: 35px;
            width: 70px;
            display: flex;
            justify-content: center;
            align-items: center;
            cursor: pointer
        }

        .wishlist {
            height: 40px;
            width: 40px;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            background-color: #eee;
            padding: 10px;
            cursor: pointer
        }

        .img-container {
            position: relative
        }

        .img-container .first {
            position: absolute;
            width: 100%
        }

        .img-container img {
            border-top-left-radius: 5px;
            border-top-right-radius: 5px
        }

        .product-detail-container {
            padding: 10px
        }

        .ratings i {
            color: #a9a6a6
        }

        .ratings span {
            color: #a9a6a6
        }

        label.radio {
            cursor: pointer
        }

        label.radio input {
            position: absolute;
            top: 0;
            left: 0;
            visibility: hidden;
            pointer-events: none
        }

        label.radio span {
            height: 25px;
            width: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 2px solid #dc3545;
            color: #dc3545;
            font-size: 10px;
            border-radius: 50%;
            text-transform: uppercase
        }

        label.radio input:checked+span {
            border-color: #dc3545;
            background-color: #dc3545;
            color: #fff
        }
        .chat .item {
            margin-bottom: 10px;
        }
        .chat .item>.offline {
            border: 2px solid #ebebeb;
        }
        .chat .item>img {
            width: 40px;
            height: 40px;
            border: 2px solid transparent;
            border-radius: 50%;
        }
        img {
            vertical-align: middle;
        }
        .chat .item>.message {
            margin-left: 55px;
            margin-top: -40px;
        }
        p {
            margin: 0 0 10px;
        }
        .chat .item>.message>.name {
            display: block;
            font-weight: 600;
        }

        .pull-right {
            float: right!important;
        }
        .text-muted {
            color: #777;
        }
    </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="content-wrapper" style="background: white">
      <div class="content-header">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12 text-center">
                  <h4>REFERENSI DESAIN GALERI</h4>
                </div>
            </div>
        </div>
      </div>
      <!-- Content Header (Page header) -->
      <!-- Main content -->
      <div class="content">
        <div class="container">
          <div class="row">
              <div class="col-lg-12">
                @include('web.partials.galeri')
                <div class="text-center" style="margin:30px">
                    <a href="/galeri" class="btn btn-warning">Lihat Lainnya</a>
                </div>
              </div>
            <!-- /.col-md-6 -->
          </div>
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content -->
    </div>
</div>
@endsection
