
@extends('web.partials.master')

@section('title') Kontak @endsection

<style>
    .contact-area {
        height: auto;
        width: 100%;
    }
    .single-icon i {
        font-size: 24px;
        width: 50px;
        height: 50px;
        border: 1px solid #444;
        line-height: 46px;
        border-radius: 50%;
        margin-bottom: 20px;
    }
    .single-icon p {
        font-size: 16px;
        line-height: 30px;
    }
    .section-headline h2 {
        display: inline-block;
        font-size: 40px;
        font-weight: 600;
        margin-bottom: 70px;
        position: relative;
        text-transform: capitalize;
    }
    p {
        margin: 0 0 15px;
        color: #444;
    }
</style>
@section('content')
<div class="container-fluid">
    <div class="content-wrapper" style="background: white">
      <div class="content-header">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12 text-center">
                  <h3>Hubungi Trilogi Project </h3>
                </div>
            </div>
        </div>
      </div>
      <!-- Content Header (Page header) -->
      <div class="content-body">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12">
                    <div id="contact" class="contact-area">
                        <div class="contact-inner area-padding">
                          <div class="contact-overly"></div>
                          <div class="container ">
                            <div class="row">
                              <!-- Start contact icon column -->
                              <div class="col-md-4 col-sm-4 col-xs-12 p-4">
                                <div class="contact-icon text-center">
                                  <div class="single-icon">
                                    <i class="fas fa-mobile-alt"></i>
                                    <p>
                                      Call: +62 822 1698 7541<br>
                                      <span>Monday-Friday (9am-5pm)</span>
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <!-- Start contact icon column -->
                              <div class="col-md-4 col-sm-4 col-xs-12 p-4">
                                <div class="contact-icon text-center">
                                  <div class="single-icon">
                                    <i class="far fa-envelope"></i>
                                    <p>
                                      Email: trilogiart93@gmail.com<br>
                                      <span>Web: www.trilogiart.com</span>
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <!-- Start contact icon column -->
                              <div class="col-md-4 col-sm-4 col-xs-12 p-4">
                                <div class="contact-icon text-center">
                                  <div class="single-icon">
                                    <i class="fas fa-map-marker-alt"></i>
                                    <p>
                                      Location: Cianjur, Jawa Barat<br>
                                      <span>43281, Indonesia</span>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">

                              <!-- Start Google Map -->
                              <div class="col-md-12">
                                <!-- Start Map -->
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31693.713096939347!2d107.22410437287598!3d-6.804587673143971!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6854a2ba90fdc9%3A0xe78efedb4b676409!2zNsKwNDcnNTMuOCJTIDEwN8KwMTInMjQuMyJF!5e0!3m2!1sid!2sid!4v1632233192545!5m2!1sid!2sid" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen="" loading="lazy"></iframe>
                                <!-- End Map -->
                              </div>
                              <!-- End Google Map -->

                              <!-- End Left contact -->
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </div>
</div>
@endsection
