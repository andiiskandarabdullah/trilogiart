@extends('web.orders.master')
@section('title') Order @endsection
@section('styles')
    <style>
        .star-rating {
        font-size: 0;
        white-space: nowrap;
        display: inline-block;
        /* width: 250px; remove this */
        height: 50px;
        overflow: hidden;
        position: relative;
        background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
        background-size: contain;
        }
        .star-rating i {
        opacity: 0;
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        /* width: 20%; remove this */
        z-index: 1;
        background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
        background-size: contain;
        }
        .star-rating input {
        -moz-appearance: none;
        -webkit-appearance: none;
        opacity: 0;
        display: inline-block;
        /* width: 20%; remove this */
        height: 100%;
        margin: 0;
        padding: 0;
        z-index: 2;
        position: relative;
        }
        .star-rating input:hover + i,
        .star-rating input:checked + i {
        opacity: 1;
        }
        .star-rating i ~ i {
        width: 40%;
        }
        .star-rating i ~ i ~ i {
        width: 60%;
        }
        .star-rating i ~ i ~ i ~ i {
        width: 80%;
        }
        .star-rating i ~ i ~ i ~ i ~ i {
        width: 100%;
        }
        ::after,
        ::before {
        height: 100%;
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        text-align: center;
        vertical-align: middle;
        }

        .star-rating.star-5 {width: 250px;}
        .star-rating.star-5 input,
        .star-rating.star-5 i {width: 20%;}
        .star-rating.star-5 i ~ i {width: 40%;}
        .star-rating.star-5 i ~ i ~ i {width: 60%;}
        .star-rating.star-5 i ~ i ~ i ~ i {width: 80%;}
        .star-rating.star-5 i ~ i ~ i ~ i ~i {width: 100%;}

        .star-rating.star-3 {width: 150px;}
        .star-rating.star-3 input,
        .star-rating.star-3 i {width: 33.33%;}
        .star-rating.star-3 i ~ i {width: 66.66%;}
        .star-rating.star-3 i ~ i ~ i {width: 100%;}
    </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="content-wrapper" style="background: white">
      <div class="content-header">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12 text-center">
                  <h3>Desain Ku</h3>
                </div>
            </div>
        </div>
      </div>
      <!-- Content Header (Page header) -->
      <div class="content-body">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Pemesanan Desain</h3>
                            <div class="card-tools">
                                <a href="/galeri" class="btn btn-secondary-outline"><small> Lihat Galeri <i class="far fa-images"></i> </small></a>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (count($orders) > 0)
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Jenis Desain</th>
                                            <th>Pembayaran</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders as $item)
                                        <tr>
                                            <td>{{$item->name}}</td>
                                            <td><span class="badge badge-success">Lunas</span></td>
                                            <td>
                                                @if ($item->status == 1)
                                                    <span class="badge badge-secondary">Menunggu Konfirmasi</span>
                                                @elseif ($item->status == 2)
                                                    <span class="badge badge-warning">Sedang dikerjakan</span>
                                                @else
                                                    <span class="badge badge-success">Selesai</span>
                                                @endif
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</td>
                                            <td>
                                                @if ($item->status == 3)
                                                <a href="{{asset($item->desain)}}" target="_blank" class="btn btn-sm  btn-outline-success rounded-circle"><i class="fas fa-download"></i></a>
                                                <a href="#" class="btn btn-sm  btn-outline-warning rounded-circle" data-toggle="modal" data-target="#ulasan-{{$item->id}}"><i class="fas fa-edit"></i></a>
                                                <!-- Modal -->
                                                <div class="modal fade" id="ulasan-{{$item->id}}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Berikan Ulasan {{$item->name}}</h5>
                                                        <a  class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </a>
                                                        </div>
                                                        <form action="{{route('ulasan', $item->id)}}" method="POST">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="form-group">
                                                                            <label for="ulasan">Ulasan</label>
                                                                            <textarea class="form-control" name="ulasan"  cols="10" rows="3" required></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="form-group">
                                                                            <label for="ulasan">Rating</label>
                                                                            <br>
                                                                            <span class="star-rating star-5">
                                                                                <input type="radio" name="rate" value="1"><i></i>
                                                                                <input type="radio" name="rate" value="2"><i></i>
                                                                                <input type="radio" name="rate" value="3"><i></i>
                                                                                <input type="radio" name="rate" value="4"><i></i>
                                                                                <input type="radio" name="rate" value="5"><i></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                                                <button type="submit" class="btn btn-primary">Kirim Ulasan</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <p class="text-center">Data pemesanan desain anda belum ada</p>
                            @endif
                        </div>
                        <div class="card-footer ">
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </div>
</div>
@endsection
