<div class="row">
    <div class="col-lg-12">
        <div class="banner">
          {{-- <img class="center-cropped" src="{{asset('dist/img/belakang.jpg')}}" alt=""> --}}
          <div class="row">
            <div class="col-lg-7">
              <section class="cd-intro" style="font-size: 62.5%;">
                <div class="cd-intro-content mask-2">
                  <div class="tittle-wrapper">
                    <div>
                      <img src="{{asset('dist/img/logo.png')}}" width="100px" height="100px" alt="">
                      <h1>Solusi Seni Digital</h1>
                      <p>Salah satu platform untuk memberikan solusi mengenai Seni Digital</p>
                      <div class="action-wrapper">
                        {{-- <a href="#0" class="cd-btn main-action">Get started</a> --}}
                        <a href="/order" class="cd-btn">Mulai Order <i class="fas fa-shopping-bag"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <div class="col-lg-5">
              <img class="center-cropped" src="{{asset('dist/img/about.png')}}" alt="">
            </div>
          </div>
        </div>
    </div>
  </div>
  <!-- /banner -->
  <!-- menu -->
