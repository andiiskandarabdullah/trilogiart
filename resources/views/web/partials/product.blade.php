<div class="content-body">
	<div class="container">
			<div class="row menu">
					<div class="col-lg-12">
						<section id="galery">
							<!--shop  area start-->
							{{-- @include('web.galery') --}}

							<div class="tab-content" id="custom-content-above-tabContent">
								<div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
									<div class="col-lg-4 col-md-4 col-12 ">
                                        @foreach ($galeri as $item)
                                            <article class="single_product">
                                                <figure>
                                                        <div class="product_thumb">
                                                                <a class="primary_img" href="product-details.html"><img src="{{asset($item->img)}}" class="galery-cropped" alt=""></a>
                                                                {{-- <div class="label_product">
                                                                        <span class="label_sale">-8%</span>
                                                                </div> --}}
                                                                <div class="action_links">
                                                                    <ul>
                                                                        <li class="add_to_cart"><a href="/order" target="_blank" title="Mulai Order"><i class="fas fa-shopping-bag"></i></a></li>

                                                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box-{{$item->id}}"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                        </div>
                                                </figure>
                                        </article>

                                        @endforeach
									</div>
								</div>
							</div>
							<br>
							<div class="tab-custom-content">
								<p class="lead mb-0"></p>
							</div>
							<!--shop  area end-->
						</section>
					</div>
			</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>

