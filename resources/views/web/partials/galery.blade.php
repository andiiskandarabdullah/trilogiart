<div class="shop_area mt-100 mb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">
              <!--shop wrapper start-->
                <!--shop toolbar start-->
                <div class="shop_toolbar_wrapper">
                    <div class="shop_toolbar_btn">

                        <button style="color: yellow" data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip" title="3"></button>

                        <button data-role="grid_4" type="button"  class=" btn-grid-4" data-toggle="tooltip" title="4"></button>

                        <button data-role="grid_list" type="button"  class="btn-list" data-toggle="tooltip" title="List"></button>
                    </div>
                    <div class=" niceselect_option">
                        <form class="select_option" action="#">
                            <select name="orderby" id="short">

                                <option selected value="1">Sort by average rating</option>
                                <option  value="2">Sort by popularity</option>
                                <option value="3">Sort by newness</option>
                                <option value="4">Sort by price: low to high</option>
                                <option value="5">Sort by price: high to low</option>
                                <option value="6">Product Name: Z</option>
                            </select>
                        </form>
                    </div>
                    <div class="page_amount">
                        <p>Showing 1–9 of 21 results</p>
                    </div>
                </div>
                <!--shop toolbar end-->
                <div class="row shop_wrapper">
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="product-details.html"><img src="{{asset('assets/img/galery/1.jpg')}}" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">-7%</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="fas fa-shopping-bag"></i></a></li>
                                            <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>
                                            <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                            <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="action_links list_action">
                                        <ul>
                                            <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="product_content grid_content">
                                    <div class="product_price_rating">
                                        <div class="product_rating">
                                          <ul>
                                              <li><a href="#"><i class="far fa-star"></i></a></li>
                                              <li><a href="#"><i class="far fa-star"></i></a></li>
                                              <li><a href="#"><i class="far fa-star"></i></a></li>
                                              <li><a href="#"><i class="far fa-star"></i></a></li>
                                              <li><a href="#"><i class="far fa-star"></i></a></li>
                                          </ul>
                                        </div>
                                        <h4 class="product_name"><a href="product-details.html">commodo augue nisi</a></h4>
                                        <div class="price_box">
                                            <span class="current_price">£69.00</span>
                                            <span class="old_price">£74.00</span>
                                        </div>
                                  </div>
                                </div>
                                <div class="product_content list_content">
                                  <div class="product_rating">
                                      <ul>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                      </ul>
                                    </div>
                                    <h4 class="product_name"><a href="product-details.html">commodo augue nisi</a></h4>
                                    <div class="price_box">
                                        <span class="current_price">£69.00</span>
                                        <span class="old_price">£74.00</span>
                                    </div>
                                    <div class="product_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco…</p>
                                    </div>
                                    <div class="action_links list_action_right">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart">Add to cart</a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>

                                    </ul>
                                    </div>
                                </div>
                            </figure>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="product-details.html"><img src="{{asset('assets/img/galery/2.jpg')}}" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">-8%</span>
                                    </div>
                                    <div class="action_links">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="fas fa-shopping-bag"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                    <div class="action_links list_action">
                                      <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="product_content grid_content">
                                    <div class="product_price_rating">
                                        <div class="product_rating">
                                          <ul>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                          </ul>
                                        </div>
                                        <h4 class="product_name"><a href="product-details.html">adipiscing cursus</a></h4>
                                        <div class="price_box">
                                            <span class="current_price">£69.00</span>
                                            <span class="old_price">£74.00</span>
                                        </div>
                                  </div>
                                </div>
                                <div class="product_content list_content">
                                  <div class="product_rating">
                                      <ul>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                      </ul>
                                    </div>
                                    <h4 class="product_name"><a href="product-details.html">adipiscing cursus</a></h4>
                                    <div class="price_box">
                                        <span class="current_price">£69.00</span>
                                        <span class="old_price">£74.00</span>
                                    </div>
                                    <div class="product_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco…</p>
                                    </div>
                                    <div class="action_links list_action_right">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart">Add to cart</a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>

                                    </ul>
                                    </div>
                                </div>
                            </figure>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="product-details.html"><img src="{{asset('assets/img/galery/3.jpg')}}" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">-9%</span>
                                    </div>
                                    <div class="action_links">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="fas fa-shopping-bag"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                    <div class="action_links list_action">
                                      <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="product_content grid_content">
                                    <div class="product_price_rating">
                                        <div class="product_rating">
                                          <ul>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                          </ul>
                                        </div>
                                        <h4 class="product_name"><a href="product-details.html">adipiscing elit</a></h4>
                                        <div class="price_box">
                                            <span class="current_price">£69.00</span>
                                            <span class="old_price">£74.00</span>
                                        </div>
                                  </div>
                                </div>
                                <div class="product_content list_content">
                                  <div class="product_rating">
                                      <ul>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                      </ul>
                                    </div>
                                    <h4 class="product_name"><a href="product-details.html">adipiscing elit</a></h4>
                                    <div class="price_box">
                                        <span class="current_price">£69.00</span>
                                        <span class="old_price">£74.00</span>
                                    </div>
                                    <div class="product_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco…</p>
                                    </div>
                                    <div class="action_links list_action_right">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart">Add to cart</a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>

                                    </ul>
                                    </div>
                                </div>
                            </figure>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="product-details.html"><img src="{{asset('assets/img/galery/4.jpg')}}" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">-6%</span>
                                    </div>
                                    <div class="action_links">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="fas fa-shopping-bag"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                    <div class="action_links list_action">
                                      <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="product_content grid_content">
                                    <div class="product_price_rating">
                                        <div class="product_rating">
                                          <ul>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                          </ul>
                                        </div>
                                        <h4 class="product_name"><a href="product-details.html">Donec eu cook</a></h4>
                                        <div class="price_box">
                                            <span class="current_price">£69.00</span>
                                            <span class="old_price">£74.00</span>
                                        </div>
                                  </div>
                                </div>
                                <div class="product_content list_content">
                                  <div class="product_rating">
                                      <ul>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                      </ul>
                                    </div>
                                    <h4 class="product_name"><a href="product-details.html">Donec eu cook</a></h4>
                                    <div class="price_box">
                                        <span class="current_price">£69.00</span>
                                        <span class="old_price">£74.00</span>
                                    </div>
                                    <div class="product_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco…</p>
                                    </div>
                                    <div class="action_links list_action_right">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart">Add to cart</a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>

                                    </ul>
                                    </div>
                                </div>
                            </figure>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="product-details.html"><img src="{{asset('assets/img/galery/5.jpg')}}" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">-9%</span>
                                    </div>
                                    <div class="action_links">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="fas fa-shopping-bag"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                    <div class="action_links list_action">
                                      <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="product_content grid_content">
                                    <div class="product_price_rating">
                                        <div class="product_rating">
                                          <ul>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                          </ul>
                                        </div>
                                        <h4 class="product_name"><a href="product-details.html">Duis pulvinar cook</a></h4>
                                        <div class="price_box">
                                            <span class="current_price">£69.00</span>
                                            <span class="old_price">£74.00</span>
                                        </div>
                                  </div>
                                </div>
                                <div class="product_content list_content">
                                  <div class="product_rating">
                                      <ul>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                      </ul>
                                    </div>
                                    <h4 class="product_name"><a href="product-details.html">Duis pulvinar cook</a></h4>
                                    <div class="price_box">
                                        <span class="current_price">£69.00</span>
                                        <span class="old_price">£74.00</span>
                                    </div>
                                    <div class="product_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco…</p>
                                    </div>
                                    <div class="action_links list_action_right">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart">Add to cart</a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>

                                    </ul>
                                    </div>
                                </div>
                            </figure>
                        </article>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="product-details.html"><img src="{{asset('assets/img/galery/6.jpg')}}" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">-5%</span>
                                    </div>
                                    <div class="action_links">
                                      <ul>
                                        <li class="add_to_cart"><a href="cart.html" title="Add to cart"><i class="fas fa-shopping-bag"></i></a></li>
                                        <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>
                                        <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                    <div class="action_links list_action">
                                      <ul>
                                        <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="far fa-eye"></i></a></li>
                                    </ul>
                                    </div>
                                </div>
                                <div class="product_content grid_content">
                                    <div class="product_price_rating">
                                        <div class="product_rating">
                                          <ul>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                            <li><a href="#"><i class="far fa-star"></i></a></li>
                                          </ul>
                                        </div>
                                        <h4 class="product_name"><a href="product-details.html">eget sagittis</a></h4>
                                        <div class="price_box">
                                            <span class="current_price">£69.00</span>
                                            <span class="old_price">£74.00</span>
                                        </div>
                                  </div>
                                </div>
                                <div class="product_content list_content">
                                  <div class="product_rating">
                                      <ul>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                        <li><a href="#"><i class="far fa-star"></i></a></li>
                                      </ul>
                                    </div>
                                    <h4 class="product_name"><a href="product-details.html">eget sagittis</a></h4>
                                    <div class="price_box">
                                        <span class="current_price">£69.00</span>
                                        <span class="old_price">£74.00</span>
                                    </div>
                                    <div class="product_desc">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco…</p>
                                    </div>
                                    <div class="action_links list_action_right">
                                        <ul>
                                            <li class="add_to_cart"><a href="cart.html" title="Add to cart">Add to cart</a></li>
                                            <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                            <li class="compare"><a href="#" title="Add to Compare"><i class="far fa-bookmark"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                            </figure>
                        </article>
                    </div>
                </div>

                <div class="shop_toolbar t_bottom">
                    <div class="pagination">
                        <ul>
                            <li class="current">1</li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="next"><a href="#">next</a></li>
                            <li><a href="#">>></a></li>
                        </ul>
                    </div>
                </div>
                <!--shop toolbar end-->
                <!--shop wrapper end-->
            </div>
            <div class="col-lg-3 col-md-12">
              <!--sidebar widget start-->
                <aside class="sidebar_widget">
                    <div class="widget_inner">
                        <div class="widget_list widget_color">
                            <h3>Select By Color</h3>
                            <ul>
                                <li>
                                    <a href="#">Black  <span>(6)</span></a>
                                </li>
                                <li>
                                    <a href="#"> Blue <span>(8)</span></a>
                                </li>
                                <li>
                                    <a href="#">Brown <span>(10)</span></a>
                                </li>
                                <li>
                                    <a href="#"> Green <span>(6)</span></a>
                                </li>
                                <li>
                                    <a href="#">Pink <span>(4)</span></a>
                                </li>

                            </ul>
                        </div>
                        <div class="widget_list tags_widget">
                            <h3>Product tags</h3>
                            <div class="tag_cloud">
                                <a href="#">Men</a>
                                <a href="#">Women</a>
                                <a href="#">Watches</a>
                                <a href="#">Bags</a>
                                <a href="#">Dress</a>
                                <a href="#">Belt</a>
                                <a href="#">Accessories</a>
                                <a href="#">Shoes</a>
                            </div>
                        </div>
                    </div>
                </aside>
                <!--sidebar widget end-->
            </div>
        </div>
    </div>
  </div>
  <div class="modal fade" id="modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="fas fa-times"></i></span>
            </button>
            <div class="modal_body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12">
                            <div class="modal_tab">
                                <div class="tab-content product-details-large">
                                    <div class="tab-pane fade show active" id="tab1" role="tabpanel" >
                                        <div class="modal_tab_img">
                                            <a href="#"><img src="{{asset('dist/img/photo2.png')}}" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2" role="tabpanel">
                                        <div class="modal_tab_img">
                                            <a href="#"><img src="{{asset('dist/img/photo1.png')}}" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab3" role="tabpanel">
                                        <div class="modal_tab_img">
                                            <a href="#"><img src="{{asset('dist/img/photo3.jpg')}}" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab4" role="tabpanel">
                                        <div class="modal_tab_img">
                                            <a href="#"><img src="{{asset('dist/img/photo4.jpg')}}" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal_tab_button">
                                    <ul class="nav product_navactive owl-carousel" role="tablist">
                                        <li >
                                            <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false"><img src="{{asset('dist/img/photo2.png')}}" alt=""></a>
                                        </li>
                                        <li>
                                             <a class="nav-link" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false"><img src="{{asset('dist/img/photo1.png')}}" alt=""></a>
                                        </li>
                                        <li>
                                           <a class="nav-link button_three" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="false"><img src="{{asset('dist/img/photo3.jpg')}}" alt=""></a>
                                        </li>
                                        <li>
                                           <a class="nav-link" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false"><img src="{{asset('dist/img/photo4.jpg')}}" alt=""></a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <div class="modal_right">
                                <div class="modal_title mb-10">
                                    <h2>Donec Ac Tempus</h2>
                                </div>
                                <div class="modal_price mb-10">
                                    <span class="new_price">$64.99</span>
                                    <span class="old_price" >$78.99</span>
                                </div>
                                <div class="modal_description mb-15">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia iste laborum ad impedit pariatur esse optio tempora sint ullam autem deleniti nam in quos qui nemo ipsum numquam, reiciendis maiores quidem aperiam, rerum vel recusandae </p>
                                </div>
                                <div class="variants_selects">
                                    <div class="variants_size">
                                       <h2>size</h2>
                                       <select class="select_option">
                                           <option selected value="1">s</option>
                                           <option value="1">m</option>
                                           <option value="1">l</option>
                                           <option value="1">xl</option>
                                           <option value="1">xxl</option>
                                       </select>
                                    </div>
                                    <div class="variants_color">
                                       <h2>color</h2>
                                       <select class="select_option">
                                           <option selected value="1">purple</option>
                                           <option value="1">violet</option>
                                           <option value="1">black</option>
                                           <option value="1">pink</option>
                                           <option value="1">orange</option>
                                       </select>
                                    </div>
                                    <div class="modal_add_to_cart">
                                        <form action="#">
                                            <input min="1" max="100" step="2" value="1" type="number">
                                            <button type="submit">add to cart</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal_social">
                                    <h2>Share this product</h2>
                                    <ul>
                                        <li class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li class="twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li class="pinterest"><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                        <li class="google-plus"><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                        <li class="linkedin"><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
