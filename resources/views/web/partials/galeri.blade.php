<div class="container-fluid mt-3 mb-3">
    <div class="row g-2">
        @if (count($galeri) > 0)
            @foreach ($galeri as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="img-container">
                            <div class="d-flex justify-content-between align-items-center p-2 first">
                                {{-- <span class="percent">-25%</span> --}}
                                <span class="wishlist"><i class="fa fa-heart"></i></span>
                            </div>
                            <a href="#"  data-toggle="modal" data-target="#modal_box-{{$item->id}}"  title="quick view">
                                <img src="{{asset($item->img)}}" class="img-fluid galery-cropped">
                            </a>
                        </div>
                        <div class="product-detail-container">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="mb-0">{{$item->name}}</h4>
                                <span class="text-warning font-weight-bold">Rp {{number_format($item->harga)}}</span>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mt-2">
                                <div class="ratings"> <i class="fa fa-star"></i> <span>{{round($item->sum_rate, 1)}}</span> </div>
                                <div class="size">
                                    <p>{{$item->caption}}</p>
                                </div>
                            </div>
                            <div class="mt-3">
                                <a class="btn btn-success btn-block" href="/order?kategori_id={{$item->id}}&desainer_id={{$item->desainer_id}}">Mulai Order</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modal_box-{{$item->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                            <div class="modal_body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="modal_tab">
                                                <div class="tab-content product-details-large">
                                                    <div class="tab-pane fade show active" id="tab1" role="tabpanel" >
                                                        <div class="modal_tab_img">
                                                            <a href="#"><img src="{{asset($item->img)}}" class="galery-cropped" alt=""></a>
                                                            <div class="row" style="padding: 50px 30px 0px 30px;">
                                                                <div class="col-12">
                                                                    <a class="btn btn-success btn-block" href="/order?galeri_id={{$item->id}}">Mulai Order</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-12">
                                            <div class="modal_right">
                                                <div class="modal_title mb-10">
                                                    <h2>{{$item->name}}</h2>
                                                </div>
                                                <div class="modal_description mb-15">
                                                    <strong><p>{{$item->desainer ? $item->desainer->name:" @desainer "}}</p></strong>
                                                </div>
                                                <div class="modal_description mb-15">
                                                    <p>{{$item->caption}}</p>
                                                </div>
                                                <div class="modal_price mb-10">
                                                    <span class="new_price">Rp {{number_format($item->harga)}}</span>
                                                </div>
                                                <div class="modal_title mb-10">
                                                    <h2>Ulasan</h2>
                                                </div>
                                                <hr>
                                                <div class="chat overflow-auto" style="height: 300px">
                                                    @if ( count($item->ulasan) > 0)
                                                        @foreach ($item->ulasan as $value)
                                                        <div class="item">
                                                            <img src="dist/img/icon-user.jpg" alt="user image" class="offline">

                                                            <p class="message">
                                                            <a href="#" class="name">
                                                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i>{{\Carbon\Carbon::parse($value->created_at)->diffForHumans()}}</small>
                                                                {{$value->customer->name}}
                                                                @for ($i = 0; $i < $value->rate; $i++)
                                                                    <i class="text-yellow far fa fa-star"></i>
                                                                @endfor
                                                            </a>
                                                            {{$value->ulasan}}
                                                            </p>
                                                        </div>
                                                        @endforeach
                                                    @else
                                                        <p class="text-center">Belum Ada Ulasan</p>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <p class="text-center">Belum Ada Referensi Desain</p>
        @endif

    </div>
</div>
