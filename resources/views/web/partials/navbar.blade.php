<nav class="main-header navbar navbar-expand-md navbar-navy navbar-dark" style="opacity: 0.8; position:fixed; width:100%; border-bottom:0px">
    <div class="container">
      <a href="/" class="navbar-brand">
        <img src="../../dist/img/trilogi-logo2.png" alt="AdminLTE Logo" class="brand-image "
             >
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item {{ Request::segment(1)==''?'active':'' }}">
            <a href="/" class="nav-link">Home</a>
          </li>
          <li class="nav-item {{ Request::segment(1)=='tentang'?'active':'' }}">
            <a href="/tentang" class="nav-link">Tentang</a>
          </li>
          <li class="nav-item {{ Request::segment(1)=='testimoni'?'active':'' }}">
            <a href="/testimoni" class="nav-link">Testimoni</a>
          </li>
          <li class="nav-item {{ Request::segment(1)=='kontak'?'active':'' }}">
            <a href="/kontak" class="nav-link">Kontak</a>
          </li>
        </ul>

      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            @auth

              @if (auth()->user()->role == 'admin')
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <strong>&nbsp;{{auth()->user()->name}}</strong> <i class="fas fa-cogs"></i>
                </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item"  href="{{ url('/admin') }}">
                        {{ __('Admin Panel') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
              </div>
              </li>
              @elseif (auth()->user()->role == 'desainer')
              <li class="nav-item dropdown">
                <a id="navbarDropdown2" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <strong>&nbsp;{{auth()->user()->name}}</strong><i class="fas fa-cogs"></i>
                </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown2">
                    <a class="dropdown-item"  href="{{ url('/desainer') }}">
                        {{ __('Desainer Panel') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
              </div>
              </li>
              @else
              <li class="nav-item dropdown">
                <a id="navbarDropdown2" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <strong>&nbsp;{{auth()->user()->name}}</strong> <i class="fas fa-cogs"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item"  href="/desainku">
                            {{ __('Desain ku') }} <i class="fas fa-shopping-cart"></i>
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </div>
            </li>
              @endif
            @else
            <li class="nav-item dropdown p-1">
                <a href="{{ route('login') }}" class="nav-link text-white">Login <i class="fas fa-sign-in-alt"></i></a>
            </li>
            <li class="nav-item dropdown p-1">
                <a href="{{ route('register') }}" class="nav-link text-yellow">Register <i class="fas fa-user-plus"></i></a>
            </li>
            @endif

      </ul>
    </div>
  </nav>
