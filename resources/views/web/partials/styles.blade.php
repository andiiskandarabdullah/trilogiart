<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/custome.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/reset.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
<script src="{{asset('dist/js/modernizr.js')}}"></script>
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<!--owl carousel min css-->
<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
<!--slick min css-->
<link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
<!--magnific popup min css-->
<link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
<!--animate css-->
<link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
<!--jquery ui min css-->
{{-- <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.min.css')}}"> --}}
<!--slinky menu css-->
<link rel="stylesheet" href="{{asset('assets/css/slinky.menu.css')}}">
<!--plugins css-->
<link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">

<!-- Main Style CSS -->
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

<!--modernizr min js here-->
{{-- <script src="{{asset('assets/js/vendor/modernizr-3.7.1.min.js')}}"></script> --}}
<!-- Google Font: Source Sans Pro -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,400' rel='stylesheet' type='text/css'>
<style>
    .galery-cropped {
      object-fit: contain; /* Do not scale the image */
      object-position: center; /* Center the image within the element */
      height: 350px;
      width: 550px;
    }
  </style>