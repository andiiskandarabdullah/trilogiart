
@extends('web.partials.master')

@section('title') Tentang @endsection
<style>
    .section-headline h2 {
        display: inline-block;
        font-size: 30px;
        font-weight: 600;
        margin-bottom: 70px;
        position: relative;
        text-transform: capitalize;
    }
    h2 {
        font-size: 38px;
        line-height: 40px;
    }
    .about-area {
        background-color: #f9f9f9;
    }
    .area-padding {
        padding: 70px 0px 80px;
    }
</style>
@section('content')
<div class="container-fluid">
    <div class="content-wrapper" style="background: white">
      <div class="content-header">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12 text-center">
                  <h3>Tentang Trilogi Project</h3>
                </div>
            </div>
        </div>
      </div>
      <!-- Content Header (Page header) -->
      <div class="content-body">
        <div class="container">
					<div class="row menu">
						<div class="col-lg-12">
							<div id="about" class="about-area area-padding">
								<div class="container">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="section-headline text-center">
												<h2>TRILOGI PROJECT</h2>
											</div>
										</div>
									</div>
									<div class="row">
										<!-- single-well start-->
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="well-left">
												<div class="single-well">
													<a href="#">
														<img src="{{asset('dist/img/kontak.png')}}" alt="">
													</a>
												</div>
											</div>
										</div>
										<!-- single-well end-->
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="well-middle">
												<div class="single-well">
													<a href="#">
														<h4 class="sec-head">Solusi Seni Digital</h4>
													</a>
													<p>
														Salah satu platform untuk memberikan solusi mengenai Seni Digital, menyediakan beberapa referensi desain yang sesuai dengan kebutuhan. dengan desainer berpengalaman akan memberikan desain-desain yang anda inginkan.
													</p>
													<ul>
														<li>
															<i class="fa fa-check text-green"></i> Referensi Desain
														</li>
														<li>
															<i class="fa fa-check text-green"></i> Desainer Berpengalaman
														</li>
														<li>
															<i class="fa fa-check text-green"></i> Pengerjaan tidak lama
														</li>
														<li>
															<i class="fa fa-check text-green"></i> Dijamin 100% selesai
														</li>
														<li>
															<i class="fa fa-check text-green"></i> Berikan ulasan desain
														</li>
													</ul>
												</div>
											</div>
										</div>
										<!-- End col-->
									</div>
								</div>
							</div>
						</div>
					</div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
    </div>
</div>
@endsection
