<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Trilogi Project |  @yield('title', '')</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('dist/img/logo.png')}}">
  @yield('styles')
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  @include('web.partials.styles')

</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  @include('web.partials.navbar')
  <!-- /.navbar -->
  <!-- banner -->
  {{-- @include('web.partials.banner') --}}
  {{-- @include('web.partials.steps') --}}



  <!-- Main content -->
  <section class="content">
      <br><br>
    @yield('content')
  </section>
  <!-- /.content -->

  <!-- Control Sidebar -->
  {{-- <aside class="control-sidebar control-sidebar-dark" style="position: fixed">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside> --}}
  <!-- /.control-sidebar -->

  @include('web.partials.footer')

  <!-- modal area start-->

<!-- modal area end-->
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
  @include('web.partials.scripts')
  @yield('scripts')
</body>
</html>
