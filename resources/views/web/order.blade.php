@extends('web.orders.master')
@section('title') Order @endsection
@section('content')
<div class="container-fluid">
    <div class="content-wrapper" style="background: white">
      <div class="content-header">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12 text-center">
                  <h3>Pemesanan Desain</h3>
                </div>
            </div>
        </div>
      </div>
      <!-- Content Header (Page header) -->
      <div class="content-body">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Form Pemesanan</h5>
                            {{-- <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            </div> --}}
                        </div>
                        <div class="card-body">
                            @auth
                            <form action="{{ route('order.store') }}"  method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="desainer_id" value="{{$desainer_id}}">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Jenis Desain</label>
                                            <select name="kategori_id" id="kategori" class="form-control" required>
                                                @foreach ($kategori as $item)
                                                    <option value="{{$item->id}}" {{$kategori_id == $item->id? 'selected':''}}>{{$item->name}} - Rp {{number_format($item->harga)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Bukti Pembayaran</label>
                                            <input type="file" class="form-control" name="lampiran" id="bukti_pembayaran" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Deskripsi</label>
                                            <textarea class="form-control" name="requirements" id="deskripsi" cols="30" rows="10" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Lampiran</label>
                                            <input type="file" class="form-control" name="bahan" id="bahan">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </form>
                            @else
                            <p class="text-center">Silahkan Login terlebih dahulu</p>
                            @endif

                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pembayaran</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-caret-down"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Melalui Bank Transfer</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <img src="{{asset('dist/img/bca.png')}}" width="70px" height="40px" alt="">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                       <label for="">No Rekening :</label>
                                       <p> 4310583401 </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Atas Nama :</label>
                                        <p> Andi Iskandar Abdulah </p>
                                     </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <img src="{{asset('dist/img/mandiri.png')}}" width="70px" height="40px" alt="">
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                       <label for="">No Rekening :</label>
                                       <p> 1320021650743 </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Atas Nama :</label>
                                        <p> Andi Iskandar Abdulah </p>
                                     </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </div>
</div>
@endsection
