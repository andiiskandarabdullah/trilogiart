
@extends('web.partials.master')

@section('title') Testimoni @endsection
@section('styles')
<link rel="stylesheet" href="{{asset('css/testimony.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
@endsection

@section('content')
<div class="container-fluid">
    <div class="content-wrapper" style="background: white">
      <div class="content-header">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12 text-center">
                  <h3>Testimoni </h3>
                </div>
            </div>
        </div>
      </div>
      <!-- Content Header (Page header) -->
      <div class="content-body">
        <div class="container">
            <div class="row menu">
                <div class="col-lg-12">
                    <div class="owl-carousel owl-theme mt-5">
                        @foreach ($testimoni as $key => $item)
                        <div class="owl-item">
                            <div class="card">
                                <div class="img-card"> <img src="{{asset('dist/img/icon-user.jpg')}}" alt=""> </div>
                                <div class="testimonial mt-4 mb-2"> {{$item->testimoni}}</div>
                                <div class="name"> {{$item->nama}} </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script>
    $(document).ready(function () {
        var silder = $(".owl-carousel");
        silder.owlCarousel({
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: false,
            items: 3,
            stagePadding: 20,
            center: true,
            nav: false,
            margin: 50,
            dots: true,
            loop: true,
            responsive: {
                0: { items: 1 },
                480: { items: 2 },
                575: { items: 2 },
                768: { items: 2 },
                991: { items: 3 },
                1200: { items: 4 }
            }
        });
    });
</script>
@endsection

