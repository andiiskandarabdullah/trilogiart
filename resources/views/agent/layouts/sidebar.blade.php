<aside class="main-sidebar elevation-0 sidebar-light text-white" style="background-color: #23b098">
    <!-- Brand Logo -->
    <a href="/" class="brand-link text-center" style="border-bottom: 1px solid #23b098">
      {{-- <img src="../../dist/img/trilogi-logo3.png"
           alt="Trilogi"
           class="brand-image"> --}}
           <span class="brand-text  text-white "><strong> Pencatatan </strong></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/admin" class="nav-link text-white">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-header">MASTER</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link text-white">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Data Master
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              	<a href="{{ route('admin.kategori') }}" class="nav-link text-white">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.kategori') }}" class="nav-link text-white">
									<i class="far fa-circle nav-icon"></i>
									<p>Channel Penjual</p>
								</a>
							</li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link text-white">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                User Management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.customer') }}" class="nav-link text-white">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.agent') }}" class="nav-link text-white">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Agent</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">APLIKASI</li>
          <li class="nav-item">
            <a href="{{ route('admin.pemesanan') }}" class="nav-link text-white">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>Transaction</p>
            </a>
          </li>
          <li class="nav-header">CASH</li>

          <li class="nav-item">
            <a href="{{ route('admin.cash') }}" class="nav-link text-white">
              <i class="nav-icon fas fa-list"></i>
              <p>Cash Activity</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
