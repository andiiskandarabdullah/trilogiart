<a href="{{route('admin.customer.edit',$id)}}" class="btn btn-sm  btn-outline-info rounded-circle"><span class="fas fa-edit"></span></a>
<a href="#" class="btn btn-sm  btn-outline-danger rounded-circle" data-toggle="modal" data-target="#delete-{{$id}}"><span class="fas fa-trash"></span></a>

<div class="modal fade" id="delete-{{$id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus data {{$name}}</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Yakin akan dihapus?
        </div>
        <div class="modal-footer justify-content-between">
        <form action="{{route('admin.customer.delete',$id)}}" method="POST">
            @csrf
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
