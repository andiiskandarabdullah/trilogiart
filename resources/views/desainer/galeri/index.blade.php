@extends('desainer.layouts.master')

@section('title') Galeri @endsection

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Galeri</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <a href="{{route('desainer.galeri.create')}}" class="btn btn-sm btn-primary btn-flat float-right"><span class="fas fa-plus"></span> Tambah Galeri </a>
                </div>
            </div>
            <table id="galeri" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Jenis Desain</th>
                <th>Caption</th>
                <th>Img</th>
                <th>Created at</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody>

            </table>
          </div>
          <!-- /.card-body -->

          <!-- /.card-footer-->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
  @endsection

  @section('scripts')

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  <script>
    $("#galeri").DataTable({
      paging: true,
      searching: false,
      ordering: false,
      autoWidth: false,
      responsive: true,
      ajax: {
        "url":"{{ route('desainer.galeri.datatable') }}",
        "type":"GET",
      },
      columns : [
        {
            data : "kategori.name",
            name : "kategori.name"
        },
        {
            data : "caption",
            name : "caption",
        },
        {
            "data": "img_asset",
            render: function (data, type, row, meta) {
                return `<img src="${data}" width="80" height="50">`;
            }
        },
        {
            data : "created_at_formated",
            name : "created_at_formated",
        },
        {
            data : 'action',
            name : 'action',
        }
      ]
    });
  </script>

  @endsection
