@extends('desainer.layouts.master')

@section('title') Create Galeri @endsection

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Create Galeri</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- form start -->
          <form role="form" action="{{route('desainer.galeri.store')}}" method="POST" id="quickForm" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Jenis Desain</label>
                    <select name="kategori_id" id="kategori_id" class="form-control">
                        @foreach ($kategori as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="nama">Image</label>
                    <input type="file" name="img" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="name">Captiom</label>
                    <textarea name="caption" class="form-control" id="name" placeholder="Enter Name" cols="30" rows="10"></textarea>
                  </div>
                  {{-- <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div> --}}
                </div>
                <!-- /.card-body -->

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           </form>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
  @endsection

