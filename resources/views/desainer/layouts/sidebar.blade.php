<aside class="main-sidebar  sidebar-light-warning">
    <!-- Brand Logo -->
    <a href="/" class="brand-link navbar-navy">
      <img src="{{asset('dist/img/trilogi-logo2.png')}}"
           alt="Trilogi"
           class="brand-image "
           >
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{ route('desainer.desainer') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          {{-- <li class="nav-item has-treeview">
            <a href="{{ route('desainer.profile') }}" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li> --}}
          <li class="nav-header">APLIKASI</li>
          <li class="nav-item">
            <a href="{{ route('desainer.galeri') }}" class="nav-link">
              <i class="nav-icon fas fa-file-image"></i>
              <p>Galeri</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('desainer.pemesanan') }}" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>Pemesanan</p>
            </a>
          </li>
          {{-- <li class="nav-header">CASH</li>
          <li class="nav-item">
            <a href="{{ route('desainer.cash') }}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>Cash Activity</p>
            </a>
          </li> --}}
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
