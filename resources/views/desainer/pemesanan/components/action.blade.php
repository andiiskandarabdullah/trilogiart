@if ($data->status == 1)
<a href="#" class="btn btn-sm  btn-outline-success rounded-circle" data-toggle="modal" data-target="#update-status-{{$data->id}}"><i class="fas fa-check"></i></a>

@endif

<a href="#" class="btn btn-sm  btn-outline-warning rounded-circle" data-toggle="modal" data-target="#upload-desain-{{$data->id}}"><i class="fas fa-upload"></i></a>
<a href="{{route('desainer.pemesanan.detail', $data->id)}}" class="btn btn-sm  btn-outline-primary rounded-circle"><i class="fas fa-eye"></i></a>
<div class="modal fade" id="update-status-{{$data->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Yakin Update status ?</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Anda Akan menerima Desain ?
        </div>
        <div class="modal-footer justify-content-between">
        <form action="{{route('desainer.pemesanan.update-status',$data->id)}}" method="POST">
            @csrf
            <button type="submit" class="btn btn-success">Konfirmasi</button>
        </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="upload-desain-{{$data->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Upload Desain</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('desainer.pemesanan.upload-desain',$data->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="desain">Upload Desain</label>
                            <input type="file" class="form-control" name="desain">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Upload</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">

        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
