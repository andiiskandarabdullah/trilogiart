@extends('desainer.layouts.master')

@section('title') Pemesanan @endsection

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Pemesanan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <table id="pemesanan" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Jenis Desain</th>
                    <th>Bukti Pembayaran</th>
                    <th>Bahan</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

              </table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">

          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
  @endsection
@section('scripts')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $("#pemesanan").DataTable({
      paging: true,
      lengthChange: true,
      searching: false,
      ordering: false,
      autoWidth: false,
      responsive: true,
      ajax: {
        "url":"{{ route('desainer.pemesanan.datatable') }}",
        "type":"GET",
      },
      columns : [
        {
            data : "customer.name",
            name : "customer.name"
        },
        {
            data : "kategori.name",
            name : "kategori.name",
        },
        {
            "data": "lampiran_src",
            render: function (data, type, row, meta) {
                return `<a href="${data}" target="_blank"><img src="${data}" width="80" height="50"></a>`;
            }
        },
        {
            "data": "bahan_src",
            render: function (data, type, row, meta) {
                if (data) {
                    return `<a href="${data}" target="_blank"><img src="${data}" width="80" height="50"></a>`;

                }else{
                    return '-'
                }
            }
        },
        {
            "data": "status",
            render: function (data, type, row, meta) {
                if (data == 1) {
                    return `<span class="badge badge-secondary">Menunggu Konfirmasi</span>`;
                }else if(data == 2){
                    return `<span class="badge badge-warning">Sedang dikerjakan</span>`;
                }else{
                    return `<span class="badge badge-success">Selesai</span>`;
                }
            }
        },
        {
            data : "created_at_formated",
            name : "created_at_formated",
        },
        {
            data : 'action',
            name : 'action',
        }
      ]
    });
  </script>
@endsection
