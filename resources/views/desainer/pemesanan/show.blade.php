@extends('desainer.layouts.master')

@section('title')Detail Pemesanan @endsection

@section('content')
    <div class="card">
        <div class="card-header">
        <h3 class="card-title"><a href="{{route('desainer.pemesanan')}}" class="text-gray"><i class="fas fa-arrow-left"></i> </a> Detail Pemesanan</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
        </div>
        <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
            <div class="row">
              <div class="col-12">
                  <div class="post">
                    <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{asset('dist/img/2.png')}}" alt="user image">
                        <span class="username">
                        <a href="#">{{$pemesanan->kategori->name}}</a>
                        </span>
                        <span class="description">
                            Tanggal Order - {{$pemesanan->created_at->format('d/m/Y')}}
                            @if ($pemesanan->status == 1)
                            <span class="badge badge-secondary">Menunggu Konfirmasi</span>
                            @elseif ($pemesanan->status == 2)
                            <span class="badge badge-warning">Sedang dikerjakan</span>
                            @else
                            <span class="badge badge-success">Selesai</span>
                            @endif
                        </span>
                    </div>
                    <!-- /.user-block -->
                    <p>
                        {{$pemesanan->requirements}}
                    </p>
                  </div>
              </div>
            </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
            <h4 class="text-primary"> <i class="fas fa-user"></i> {{$pemesanan->customer->name}}</h4>
            {{-- <p class="text-muted">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p> --}}
            <br>
            <div class="text-muted">
                <p class="text-sm">Email
                <b class="d-block">{{$pemesanan->customer->email}}</b>
                </p>
                <p class="text-sm">Phone Number
                <b class="d-block">{{$pemesanan->customer->phone_number?$pemesanan->customer->name:'-'}}</b>
                </p>
            </div>

            <h5 class="mt-5 text-muted">Project files</h5>
            <ul class="list-unstyled">
                <li>
                <a href="{{asset($pemesanan->bahan)}}" target="_blank" class="btn-link text-secondary"><i class="fas fa-download"></i> Lampiran</a>
                @if ($pemesanan->bahan)
                    <a href="#" class="text-success"><i class="fas fa-check"></i></a>
                @else
                    <a href="#" class="text-danger"><i class="fas fa-times"></i></a>
                @endif
                </li>
                <li>
                <a href="{{asset($pemesanan->lampiran)}}" target="_blank"  class="btn-link text-secondary"><i class="fas fa-eye"></i> Bukti Pembayaran</a>
                @if ($pemesanan->lampiran)
                    <a href="#" class="text-success"><i class="fas fa-check"></i></a>
                @else
                    <a href="#" class="text-danger"><i class="fas fa-times"></i></a>
                @endif
                </li>

            </ul>
            <div class="text-center mt-5 mb-3">
							@if ($pemesanan->status < 2)
							<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#update-status-{{$pemesanan->id}}">Konfirmasi</a>
							@endif
                <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#upload-desain-{{$pemesanan->id}}">Upload Desain</a>
            </div>
            </div>
        </div>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="modal fade" id="update-status-{{$pemesanan->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Yakin Update status ?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Anda Akan menerima Desain ?
            </div>
            <div class="modal-footer justify-content-between">
            <form action="{{route('desainer.pemesanan.update-status',$pemesanan->id)}}" method="POST">
                @csrf
                <button type="submit" class="btn btn-success">Konfirmasi</button>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      <div class="modal fade" id="upload-desain-{{$pemesanan->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Upload Desain</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{route('desainer.pemesanan.upload-desain',$pemesanan->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="desain">Upload Desain</label>
                                <input type="file" class="form-control" name="desain">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning">Upload</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">

            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@endsection
