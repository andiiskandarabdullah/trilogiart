<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <style>
    .img-login, .img-login-logo {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    @media (max-width: 500px) {
        .img-login {
            display: none;
        }
    }
    @media (min-width: 500px) {
        .img-login-logo {
            display: none;
        }
    }
  </style>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body style="background-color: #fed651!important">
    <div class="row " style="margin: 50px;">
        <div class="col-lg-8" style="background-image: url('{{asset('dist/img/login.gif')}}')">

        </div>
        <div class="col-lg-4" style="background-color: rgb(32, 51, 90);height:auto;padding:20px;">
            <div  style="background-color:  rgb(32, 51, 90)">
                <div class="" style="background-color:  rgb(32, 51, 90)">
                  <div class="card-body login-card-body" style="background-color:  rgb(32, 51, 90);">
                    <img class="img-login"  src="{{asset('dist/img/trilogi-logo2.png')}}" width="250px"  alt="">
                    <img class="img-login-logo"  src="{{asset('dist/img/logo.png')}}" width="100px"  alt="">
                    {{-- <h3 class="login-box-msg" style="color:#fed651"><strong>TRILOGI ART</strong> </h3> --}}
                    <br>
                    <p class="login-box-msg" style="color:white">Sign up to start your session</p>

                    <form method="POST" action="{{ route('storeRegistrasi') }}" id="quickForm">
                      @csrf

                      <div class="form-group row">
                        <div class="col-md-12">
                            <input id="name" placeholder="Nama Lengkap" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                      <div class="form-group row">
                          <div class="col-md-12">
                              <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                              @error('email')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <div class="col-md-12">
                              <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                              @error('password')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                        <div class="col-md-12">
                            <input id="password_confirmation" placeholder="Konfirmasi Password" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="current-password">

                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                      <div class="form-group row">
                          <div class="col-md-12">
                              <button type="submit" class="btn btn-primary col-lg-12">
                                  {{ __('Register') }}
                              </button>
                          </div>
                      </div>
                  </form>
                    <br>
                    <hr>

                    {{-- <p class="mb-1">
                      <a href="#">I forgot my password</a>
                    </p> --}}
                    <p class="mb-0">
                      <a href="{{ route('login') }}" class="text-center">Sudah Punya Akun? Login</a>
                    </p>
                  </div>
                  <!-- /.login-card-body -->
                </div>
              </div>
        </div>
      </div>

<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
      $.validator.setDefaults({
        submitHandler: function () {

        }
      });
      $('#quickForm').validate({
        rules: {
          name: {
            required: true,
          },
        },
        messages: {
          name: "Please enter a name"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });
    });
    </script>
</body>
</html>

