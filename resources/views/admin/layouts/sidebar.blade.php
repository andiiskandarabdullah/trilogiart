<aside class="main-sidebar sidebar-light-warning">
    <!-- Brand Logo -->
    <a href="/" class="brand-link  navbar-warning">
      <img src="../../dist/img/trilogi-logo3.png"
           alt="Trilogi"
           class="brand-image">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/admin" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-header">MASTER</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Data Master
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a href="{{ route('admin.kategori') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.testimoni') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Testimoni</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                User Management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.user-admin') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Admin</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.desainer') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Desainer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.customer') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer</p>
                </a>
              </li>
              {{-- <li class="nav-item">
                <a href="{{ route('admin.agent') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Agent</p>
                </a>
              </li> --}}
            </ul>
          </li>
          {{-- <li class="nav-header">APLIKASI</li>
          <li class="nav-item">
            <a href="{{ route('admin.galeri') }}" class="nav-link">
              <i class="nav-icon fas fa-file-image"></i>
              <p>Galeri</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.pemesanan') }}" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>Pemesanan</p>
            </a>
          </li> --}}
          <li class="nav-header">CASH</li>

          <li class="nav-item">
            <a href="{{ route('admin.cash') }}" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>Cash Activity</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

