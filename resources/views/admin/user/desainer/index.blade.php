@extends('admin.layouts.master')

@section('title') Desainer User @endsection

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Desainer User</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-12">
                <a href="{{route('admin.desainer.create')}}" class="btn btn-sm btn-primary btn-flat float-right"><span class="fas fa-plus"></span> Tambah Desainer </a>
                </div>
            </div>
            <table id="desainer" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>Created at</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody>

            </table>
          </div>
          <!-- /.card-body -->

          <!-- /.card-footer-->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
  @endsection

  @section('scripts')

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  <script>
    $("#desainer").DataTable({
      paging: true,
      searching: false,
      responsive: true,
      ajax: {
        "url":"{{ route('admin.desainer.datatable') }}",
        "type":"GET",
      },
      columns : [
        {
            data : "name",
            name : "name"
        },
        {
            data : "email",
            name : "email"
        },
        {
            data : "created_at_formated",
            name : "created_at_formated",
        },
        {
            data : "action",
            name : "action",
        }
      ]
    });
  </script>

  @endsection
