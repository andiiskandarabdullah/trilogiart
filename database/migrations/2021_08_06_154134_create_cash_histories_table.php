<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cash_id')->nullable();
            $table->bigInteger('transaction_id')->nullable();
            $table->double('previous_value', 20, 8)->nullable();
            $table->double('current_value', 20, 8)->nullable();
            $table->boolean('in')->nullable()->default(false);
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_histories');
    }
}
