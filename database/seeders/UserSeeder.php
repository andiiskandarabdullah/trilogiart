<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Administrator';
        $user->email = 'admin@admin.com';
        $user->role = 'admin';
        $user->password = \Illuminate\Support\Facades\Hash::make('1234qwer');
        $user->save();
    }
}
