<?php

use Illuminate\Support\Facades\Route;
use App\Models\Galery;
use App\Models\Testimony;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route web
Route::get('/', function () {
    $galeri = Galery::with('desainer','ulasan')
    ->leftjoin('kategori','kategori.id', '=', 'galeries.kategori_id')
    ->orderBy('galeries.created_at', 'desc')
    ->limit(3)
    ->get();
    return view('home', compact('galeri'));
});
Route::get('/kontak', function () {
    return view('web.kontak');
});
Route::get('/tentang', function () {
    return view('web.tentang');
});
Route::get('/testimoni', function () {
    $testimoni = Testimony::all();
    return view('web.testimoni', compact('testimoni'));
});


//route auth
Auth::routes();

//route middleware menu
Route::middleware('auth', 'admin')->group(function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin');

        Route::get('/kategori', [App\Http\Controllers\Admin\KategoriController::class, 'index'])->name('kategori');
        Route::get('/kategori/create', [App\Http\Controllers\Admin\KategoriController::class, 'create'])->name('kategori.create');
        Route::get('/kategori/edit/{id}', [App\Http\Controllers\Admin\KategoriController::class, 'edit'])->name('kategori.edit');
        Route::post('/kategori/store', [App\Http\Controllers\Admin\KategoriController::class, 'store'])->name('kategori.store');
        Route::post('/kategori/update/{id}', [App\Http\Controllers\Admin\KategoriController::class, 'update'])->name('kategori.update');
        Route::post('/kategori/delete/{id}', [App\Http\Controllers\Admin\KategoriController::class, 'destroy'])->name('kategori.delete');
        Route::get('/kategori/datatable', [App\Http\Controllers\Admin\KategoriController::class, 'datatable'])->name('kategori.datatable');


        Route::get('/testimoni', [App\Http\Controllers\Admin\TestimoniController::class, 'index'])->name('testimoni');
        Route::get('/testimoni/create', [App\Http\Controllers\Admin\TestimoniController::class, 'create'])->name('testimoni.create');
        Route::get('/testimoni/edit/{id}', [App\Http\Controllers\Admin\TestimoniController::class, 'edit'])->name('testimoni.edit');
        Route::post('/testimoni/store', [App\Http\Controllers\Admin\TestimoniController::class, 'store'])->name('testimoni.store');
        Route::post('/testimoni/update/{id}', [App\Http\Controllers\Admin\TestimoniController::class, 'update'])->name('testimoni.update');
        Route::post('/testimoni/delete/{id}', [App\Http\Controllers\Admin\TestimoniController::class, 'destroy'])->name('testimoni.delete');
        Route::get('/testimoni/datatable', [App\Http\Controllers\Admin\TestimoniController::class, 'datatable'])->name('testimoni.datatable');

        //user management
        Route::get('/user-admin', [App\Http\Controllers\Admin\UserController::class, 'indexAdmin'])->name('user-admin');
        Route::get('/create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('create');
        Route::get('/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('edit');
        Route::post('/store', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('store');
        Route::post('/update/{id}', [App\Http\Controllers\Admin\UserController::class, 'update'])->name('update');
        Route::post('/delete/{id}', [App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('delete');
        Route::get('/datatable', [App\Http\Controllers\Admin\UserController::class, 'datatableAdmin'])->name('datatable');

        Route::get('/desainer', [App\Http\Controllers\Admin\UserController::class, 'indexDesainer'])->name('desainer');
        Route::get('/desainer/create', [App\Http\Controllers\Admin\UserController::class, 'createDesainer'])->name('desainer.create');
        Route::get('/desainer/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'editDesainer'])->name('desainer.edit');
        Route::post('/desainer/store', [App\Http\Controllers\Admin\UserController::class, 'storeDesainer'])->name('desainer.store');
        Route::post('/desainer/update/{id}', [App\Http\Controllers\Admin\UserController::class, 'updateDesainer'])->name('desainer.update');
        Route::post('/desainer/delete/{id}', [App\Http\Controllers\Admin\UserController::class, 'destroyDesainer'])->name('desainer.delete');
        Route::get('/desainer/datatable', [App\Http\Controllers\Admin\UserController::class, 'datatableDesainer'])->name('desainer.datatable');

        Route::get('/customer', [App\Http\Controllers\Admin\UserController::class, 'indexCustomer'])->name('customer');
        Route::get('/customer/create', [App\Http\Controllers\Admin\UserController::class, 'createCustomer'])->name('customer.create');
        Route::get('/customer/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'editCustomer'])->name('customer.edit');
        Route::post('/customer/store', [App\Http\Controllers\Admin\UserController::class, 'storeCustomer'])->name('customer.store');
        Route::post('/customer/update/{id}', [App\Http\Controllers\Admin\UserController::class, 'updateCustomer'])->name('customer.update');
        Route::post('/customer/delete/{id}', [App\Http\Controllers\Admin\UserController::class, 'destroyCustomer'])->name('customer.delete');
        Route::get('/customer/datatable', [App\Http\Controllers\Admin\UserController::class, 'datatableCustomer'])->name('customer.datatable');

        Route::get('/agent', [App\Http\Controllers\Admin\UserController::class, 'indexAgent'])->name('agent');
        Route::get('/agent/create', [App\Http\Controllers\Admin\UserController::class, 'createAgent'])->name('agent.create');
        Route::get('/agent/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'editAgent'])->name('agent.edit');
        Route::post('/agent/store', [App\Http\Controllers\Admin\UserController::class, 'storeAgent'])->name('agent.store');
        Route::post('/agent/update/{id}', [App\Http\Controllers\Admin\UserController::class, 'updateAgent'])->name('agent.update');
        Route::post('/agent/delete/{id}', [App\Http\Controllers\Admin\UserController::class, 'destroyAgent'])->name('agent.delete');
        Route::get('/agent/datatable', [App\Http\Controllers\Admin\UserController::class, 'datatableAgent'])->name('agent.datatable');

        Route::get('/galeri', [App\Http\Controllers\Admin\GaleriController::class, 'index'])->name('galeri');


        Route::get('/pemesanan', [App\Http\Controllers\Admin\PemesananController::class, 'index'])->name('pemesanan');


        Route::get('/cash', [App\Http\Controllers\Admin\CashController::class, 'index'])->name('cash');
    });
});

Route::middleware('auth', 'desainer')->group(function () {
    Route::get('/karyaku', function () {
        return view('web.karyaku');
    });

    Route::group(['prefix' => 'desainer', 'as' => 'desainer.'], function () {
        Route::get('/', [App\Http\Controllers\Desainer\HomeController::class, 'index'])->name('desainer');


        Route::get('/profile', [App\Http\Controllers\Desainer\ProfileController::class, 'index'])->name('profile');


        Route::get('/galeri', [App\Http\Controllers\Desainer\GaleriController::class, 'index'])->name('galeri');
        Route::get('/galeri/datatable', [App\Http\Controllers\Desainer\GaleriController::class, 'datatable'])->name('galeri.datatable');
        Route::get('/galeri/create', [App\Http\Controllers\Desainer\GaleriController::class, 'create'])->name('galeri.create');
        Route::get('/galeri/edit/{id}', [App\Http\Controllers\Desainer\GaleriController::class, 'edit'])->name('galeri.edit');
        Route::post('/galeri/store', [App\Http\Controllers\Desainer\GaleriController::class, 'store'])->name('galeri.store');
        Route::post('/galeri/update/{id}', [App\Http\Controllers\Desainer\GaleriController::class, 'update'])->name('galeri.update');
        Route::post('/galeri/delete/{id}', [App\Http\Controllers\Desainer\GaleriController::class, 'destroy'])->name('galeri.delete');


        Route::get('/pemesanan', [App\Http\Controllers\Desainer\PemesananController::class, 'index'])->name('pemesanan');
        Route::get('/pemesanan/datatable', [App\Http\Controllers\Desainer\PemesananController::class, 'datatable'])->name('pemesanan.datatable');
        Route::get('/pemesanan/detail/{id}', [App\Http\Controllers\Desainer\PemesananController::class, 'getDetail'])->name('pemesanan.detail');
        Route::post('/pemesanan/update-status/{id}', [App\Http\Controllers\Desainer\PemesananController::class, 'updateStatus'])->name('pemesanan.update-status');
        Route::post('/pemesanan/upload-desain/{id}', [App\Http\Controllers\Desainer\PemesananController::class, 'uploadDesain'])->name('pemesanan.upload-desain');


        Route::get('/cash', [App\Http\Controllers\Desainer\CashController::class, 'index'])->name('cash');
    });
});

Route::get('/order', [App\Http\Controllers\OrderController::class, 'index']);
Route::get('/galeri', [App\Http\Controllers\GaleryController::class, 'index']);
Route::post('/ulasan/{id}', [App\Http\Controllers\OrderController::class, 'sendUlasan'])->name('ulasan');
Route::post('/storeRegister', [App\Http\Controllers\Auth\RegisterController::class, 'registrasi'])->name('storeRegistrasi');
Route::middleware('auth', 'customer')->group(function () {
    Route::get('/desainku', [App\Http\Controllers\OrderController::class, 'getOrder'])->name('desainku');

    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::post('/create', [App\Http\Controllers\OrderController::class, 'store'])->name('store');
    });
});
