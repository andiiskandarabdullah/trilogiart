<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama',
        'testimoni',
        'img',
    ];

    protected $dates = ['created_at'];
    protected $appends = ['action','created_at_formated'];

    public function getActionAttribute()
    {
        return view('admin.testimoni.components.action',['data'=>$this])->render();
    }

    public function getCreatedAtFormatedAttribute()
    {
        return $this->created_at->format('d-m-Y H:i:s');
    }
}
