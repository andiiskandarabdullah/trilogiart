<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'kategori_id',
        'desainer_id',
        'order_id',
        'costumer_id',
        'amount',
        'status'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }
    public function desainer()
    {
        return $this->belongsTo(User::class, 'desainer_id');
    }
    public function customer()
    {
        return $this->belongsTo(User::class, 'costumer_id');
    }
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
