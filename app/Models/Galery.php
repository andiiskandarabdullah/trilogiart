<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Galery extends Model
{
    use HasFactory;

    protected $fillable = [
        'desainer_id',
        'img',
        'caption',
        'kategori_id',
        'sum_rate'
    ];
    protected $dates = ['created_at'];
    protected $appends = ['action','created_at_formated','img_asset','rate'];

    public static function boot()
    {
        parent::boot();

        self::saving(function ($galery) {
            $galeryOld = Galery::where('kategori_id', $galery->kategori_id)->first();
            $galery->sum_rate = $galeryOld->sum_rate;
        });
    }
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function desainer()
    {
        return $this->belongsTo(User::class, 'desainer_id');
    }
    public function ulasan()
    {
        return $this->hasMany(Ulasan::class,'kategori_id', 'kategori_id');
    }

    public function getActionAttribute()
    {
        return view('desainer.galeri.components.action',['data'=>$this])->render();
    }

    public function getCreatedAtFormatedAttribute()
    {
        return $this->created_at->format('d-m-Y H:i:s');
    }

    public function getImgAssetAttribute()
    {
        return asset($this->img);
    }

    public function getRateAttribute()
    {
        $ulasan = Ulasan::where('kategori_id', $this->kategori_id)->get();
        $rate = 0;
        if (count($ulasan) > 0) {
            foreach ($ulasan as $value) {
                $rate += $value->rate;
            }
            $rate = (float)$rate / (float)count($ulasan);
        }
        return $rate;
    }

    public function scopeFilter($query, $request)
    {
        if (!empty($request->all())) {
            if ($request->has('kategori_id') && !empty($request->kategori_id)) {
                $query->where('kategori_id', $request->kategori_id);
            }
            if ($request->has('caption') && !empty($request->caption)) {
                $query->where('caption', 'LIKE', '%'.$request->caption.'%');
            }
            if ($request->has('desainer_id') && !empty($request->desainer_id)) {
                $query->where('desainer_id', $request->desainer_id);
            }
            if ($request->has('harga') && !empty($request->harga)) {
                if ($request->harga == 'terendah') {
                    $query->orderBy('harga','asc');
                }else{
                    $query->orderBy('harga','desc');
                }
            }
            if ($request->has('sortBy') && !empty($request->sortBy)) {
                if ($request->sortBy == 'terpopuler') {
                    $query->orderBy('sum_rate','desc');
                }
            }
        }else{
            $query->orderBy('galeries.created_at','desc');
        }
    }
}
