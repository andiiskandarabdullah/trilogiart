<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'cash_id',
        'transaction_id',
        'previous_value',
        'current_value',
        'in',
        'type'
    ];

    protected $dates = ['created_at'];

    public function cash()
    {
        return $this->belongsTo(User::class, 'cash_id');
    }
}
