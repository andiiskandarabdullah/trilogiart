<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desainer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'code',
        'nama_lengkap',
        'alamat',
        'no_hp',
        'keahlian',
        'img',
        'sumTask'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
