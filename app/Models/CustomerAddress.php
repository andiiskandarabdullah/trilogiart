<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'address',
        'desa',
        'kecamatan',
        'provinsi',
        'kode_pos'
    ];

    protected $dates = ['created_at'];

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }
}
