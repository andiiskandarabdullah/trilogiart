<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'channel_id',
        'customer_id',
        'purchase_price',
        'selling_price',
        'status',
        'desc'
    ];

    protected $dates = ['created_at'];

    public function category()
    {
        return $this->belongsTo(User::class, 'category_id');
    }

    public function channel()
    {
        return $this->belongsTo(User::class, 'channel_id');
    }

    public function custome()
    {
        return $this->belongsTo(User::class, 'custome_id');
    }
}
