<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    use HasFactory;

    protected $table = 'ulasan';
    protected $fillable = [
        'customer_id',
        'kategori_id',
        'order_id',
        'rate',
        'ulasan'
    ];

    protected $dates = ['created_at'];
    protected $with = ['customer'];
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
