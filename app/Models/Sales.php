<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;

    protected $fillable = [
        'galeri_id',
        'price',
        'desc',
        'diskon',
        'status',
    ];

    public function galeri()
    {
        return $this->belongsTo(Galery::class, 'galeri_id');
    }
}