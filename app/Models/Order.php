<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'desainer_id',
        'kategori_id',
        'requirements',
        'lampiran',
        'desain',
        'bahan',
        'status',
    ];
    protected $dates = ['created_at'];
    protected $appends = ['action','created_at_formated','lampiran_src', 'desain_src', 'bahan_src'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
    public function desainer()
    {
        return $this->belongsTo(Desainer::class, 'desainer_id');
    }
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function getActionAttribute()
    {
        return view('desainer.pemesanan.components.action',['data'=>$this])->render();
    }
    public function ulasan()
    {
        return $this->hasMany(Ulasan::class,'kategori_id', 'kategori_id');
    }
    public function getCreatedAtFormatedAttribute()
    {
        return $this->created_at->format('d-m-Y H:i:s');
    }
    public function getLampiranSrcAttribute()
    {
        if ($this->lampiran) {
            return asset($this->lampiran);
        }
        return '';
    }
    public function getDesainSrcAttribute()
    {
        if ($this->desain) {
            return asset($this->desain);
        }
        return '';
    }

    public function getBahanSrcAttribute()
    {
        if ($this->bahan) {
            return asset($this->bahan);
        }
        return '';
    }
}
