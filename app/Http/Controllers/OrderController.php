<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Desainer;
use App\Models\Ulasan;
use App\Models\Galery;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $kategori = Kategori::all();
        $kategori_id =  $request->has('kategori_id') ? $request->kategori_id : '';
        $desainer_id =  $request->has('desainer_id') ? $request->desainer_id : '';
        return view('web.order', compact('kategori', 'kategori_id','desainer_id'));
    }

    public function getOrder(Request $request)
    {
        $customer = Customer::where('user_id', auth()->user()->id)->first();
        $orders = Order::select('orders.id','kategori.name','orders.created_at', 'orders.status','orders.desain')
        ->leftjoin('kategori', 'kategori.id' , '=', 'orders.kategori_id')
        ->where('customer_id', $customer->id)
        ->orderBy('orders.created_at','desc')
        ->get();
        return view('web.orderanku', compact('orders'));
    }
    public function store(Request $request)
    {
        $rules = [
            'kategori_id' => 'required',
            'requirements' => 'required',
            'lampiran' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();
            $desainer_id = '';
            if ($request->has('desainer_id') && $request->desainer_id != '') {
                $desainer_id = $request->desainer_id;
            }else{
                $desainer = Desainer::orderBy('sumTask', 'asc')->first();
                $desainer_id = $desainer->user_id;
                $desainer->sumTask = $desainer->sumTask + 1;
                $desainer->save();
            }
            $img = '';
            $imageUrl = $request->file('lampiran');
            if ($imageUrl){
                $imageName = time().'.'.$request->file('lampiran')->getClientOriginalExtension();
                $request->file('lampiran')->move(public_path('/assets/dist/images'), $imageName);
                $img = '/assets/dist/images/'.$imageName;
            }
            $bahan = '';
            $imageUrl = $request->file('bahan');
            if ($imageUrl){
                $imageName = time().'.'.$request->file('bahan')->getClientOriginalExtension();
                $request->file('bahan')->move(public_path('/assets/dist/images'), $imageName);
                $bahan = '/assets/dist/images/'.$imageName;
            }
            $data = new Order();
            $customer = Customer::where('user_id', auth()->user()->id)->first();
            $input = [
                'customer_id' => $customer->id,
                'desainer_id' => $desainer_id,
                'status' => 1,
                'lampiran' => $img,
                'kategori_id' => $request->kategori_id,
                'requirements' => $request->requirements,
                'bahan' => $bahan,
            ];
            $data->fill($input);
            $data->save();

            DB::commit();
            return redirect('/desainku')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function sendUlasan(Request $request, $id)
    {
        $rules = [
            'ulasan' => 'required',
            'rate' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();
            $dataOrder = Order::where('id', $id)->first();
            $data = new Ulasan();
            $input = [
                'customer_id' => $dataOrder->customer_id,
                'kategori_id'=> $dataOrder->kategori_id,
                'order_id' => $dataOrder->id,
                'rate'=> $request->rate,
                'ulasan'=> $request->ulasan
            ];
            $data->fill($input);
            $data->save();
            $sumRate = Ulasan::where('kategori_id', $dataOrder->kategori_id)->sum('rate');
            $countRate = Ulasan::where('kategori_id', $dataOrder->kategori_id)->count('rate');
            $sumRateGalery = round(((int)$sumRate / (int)$countRate),1);
            Galery::where('kategori_id', $dataOrder->kategori_id)->update([
                'sum_rate' => $sumRateGalery
            ]);
            DB::commit();
            return redirect('/desainku')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}
