<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Galery;
use App\Models\Kategori;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Desainer;
use App\Models\Ulasan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class GaleryController extends Controller
{
    public function index(Request $request)
    {
        $galeri = Galery::with('desainer','ulasan')
        ->leftjoin('kategori','kategori.id', '=', 'galeries.kategori_id')
        ->filter($request)->get();

        $kategori = Kategori::get();
        $desainer = Desainer::with('user')->get();
        return view('web.orders.index', compact('galeri','kategori','desainer'));
    }
}
