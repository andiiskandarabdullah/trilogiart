<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Desainer;
use App\Models\Customer;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Artisan;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function indexAdmin()
    {
        return view('admin.user.admin.index');
    }

    public function create(Request $request)
    {
        return view('admin.user.admin.create');
    }

    public function edit(Request $request,$id)
    {
        return view('admin.user.admin.edit',['data'=>User::where('id',$id)->first()]);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = new User();
            $request->merge(['password'=> Hash::make($request->password)]);
            $input = $request->all();
            $input['role'] = 'admin';
            $data->fill($input);
            $data->save();

            DB::commit();
            return Redirect::route('admin.user-admin')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update(Request $request,$id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = User::where('id', $id);
            $input = [
                'name'=>$request->name,
                'email'=>$request->email
            ];
            $data->update($input);

            DB::commit();
            return Redirect::route('admin.user-admin')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            User::where('id',$id)->delete();
            DB::commit();
            return Redirect::route('admin.user-admin')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function datatableAdmin()
    {
        return $data = DataTables::of(User::where('role','admin')->orderBy('created_at', 'desc')->get())
        ->make(true);
        $this->make($data);
    }

    public function indexDesainer()
    {
        return view('admin.user.desainer.index');
    }

    public function createDesainer(Request $request)
    {
        return view('admin.user.desainer.create');
    }

    public function editDesainer(Request $request,$id)
    {
        return view('admin.user.desainer.edit',['data'=>User::where('id',$id)->first()]);
    }

    public function storeDesainer(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = new User();
            $request->merge(['password'=> Hash::make($request->password)]);
            $input = $request->all();
            $input['role'] = 'desainer';
            $data->fill($input);
            $data->save();
            Desainer::create([
                'user_id' => $data->id,
                'nama_lengkap' => $request->name,
            ]);
            DB::commit();
            return Redirect::route('admin.desainer')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function updateDesainer(Request $request,$id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = User::where('id', $id);
            $input = [
                'name'=>$request->name,
                'email'=>$request->email
            ];
            $data->update($input);

            DB::commit();
            return Redirect::route('admin.desainer')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroyDesainer($id)
    {
        try {
            DB::beginTransaction();
            User::where('id',$id)->delete();
            DB::commit();
            return Redirect::route('admin.desainer')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function datatableDesainer()
    {
        $model = User::where('role','desainer')->orderBy('created_at', 'desc')->get();
        return $data = DataTables::of($model)
        ->editColumn('action', 'admin.user.desainer.components.action')
        ->make(true);
        $this->make($data);
    }

    public function indexCustomer()
    {
        return view('admin.user.customer.index');
    }

    public function createCustomer(Request $request)
    {
        return view('admin.user.customer.create');
    }

    public function editCustomer(Request $request,$id)
    {
        return view('admin.user.customer.edit',['data'=>User::where('id',$id)->first()]);
    }

    public function storeCustomer(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = new User();
            $request->merge(['password'=> Hash::make($request->password)]);
            $input = $request->all();
            $input['role'] = 'customer';
            $data->fill($input);
            $data->save();
            Customer::create([
                'user_id' => $data->id,
                'name' => $request->name,
                'email' => $request->email,
            ]);
            DB::commit();
            return Redirect::route('admin.customer')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function updateCustomer(Request $request,$id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = User::where('id', $id);
            $input = [
                'name'=>$request->name,
                'email'=>$request->email
            ];
            $data->update($input);

            DB::commit();
            return Redirect::route('admin.customer')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroyCustomer($id)
    {
        try {
            DB::beginTransaction();
            User::where('id',$id)->delete();
            DB::commit();
            return Redirect::route('admin.customer')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function datatableCustomer()
    {
        return $data = DataTables::of(User::where('role','customer')->orderBy('created_at', 'desc')->get())
        ->editColumn('action', 'admin.user.customer.components.action')
        ->make(true);
        $this->make($data);
    }
    public function indexAgent()
    {
        return view('admin.user.agent.index');
    }

    public function createAgent(Request $request)
    {
        return view('admin.user.agent.create');
    }

    public function editAgent(Request $request,$id)
    {
        return view('admin.user.agent.edit',['data'=>User::where('id',$id)->first()]);
    }
    public function storeAgent(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = new User();
            $request->merge(['password'=> Hash::make($request->password)]);
            $input = $request->all();
            $input['role'] = 'agent';
            $data->fill($input);
            $data->save();
            if ($data) {
                Agent::create([
                    'user_id' => $data->id,
                    'name' => $data->name
                ]);
            }
            DB::commit();
            return Redirect::route('admin.agent')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function updateAgent(Request $request,$id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = User::where('id', $id);
            $input = [
                'name'=>$request->name,
                'email'=>$request->email
            ];
            $data->update($input);

            DB::commit();
            return Redirect::route('admin.customer')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroyAgent($id)
    {
        try {
            DB::beginTransaction();
            $user = User::where('id',$id)->delete();
            if ($user) {
                Agent::where('user_id', $id)->delete();
            }
            DB::commit();
            return Redirect::route('admin.customer')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function datatableAgent()
    {
        return $data = DataTables::of(User::where('role','agent')->orderBy('created_at', 'desc')->get())
        ->editColumn('action', 'admin.user.agent.components.action')
        ->make(true);
        $this->make($data);
    }
}
