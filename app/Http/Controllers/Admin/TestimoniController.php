<?php

namespace App\Http\Controllers\Admin;

use App\Models\Testimony;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Artisan;
use Yajra\DataTables\Facades\DataTables;

class TestimoniController extends Controller
{
    public function index()
    {
        return view('admin.testimoni.index');
    }
    public function create(Request $request)
    {
        return view('admin.testimoni.create');
    }

    public function edit(Request $request,$id)
    {
        return view('admin.testimoni.edit',['data'=>Testimony::where('id',$id)->first()]);
    }

    public function store(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'testimoni' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = new Testimony();
            $input = $request->all();
            $data->fill($input);
            $data->save();

            DB::commit();
            return Redirect::route('admin.testimoni')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update(Request $request,$id)
    {
        $rules = [
            'nama' => 'required',
            'testimoni' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = Testimony::where('id', $id);
            $input = [
                'nama'=>$request->nama,
                'testimoni'=>$request->testimoni
            ];
            $data->update($input);

            DB::commit();
            return Redirect::route('admin.testimoni')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Testimony::where('id',$id)->delete();
            DB::commit();
            return Redirect::route('admin.testimoni')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
    public function datatable()
    {
        return $data = DataTables::of(Testimony::orderBy('created_at', 'desc')->get())
        ->make(true);
        $this->make($data);
    }
}
