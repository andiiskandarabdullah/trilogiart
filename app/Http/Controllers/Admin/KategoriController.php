<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class KategoriController extends Controller
{
    public function index()
    {
        return view('admin.kategori.index');
    }

    public function create(Request $request)
    {
        return view('admin.kategori.create');
    }

    public function edit(Request $request,$id)
    {
        return view('admin.kategori.edit',['data'=>Kategori::where('id',$id)->first()]);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'harga' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = new Kategori();
            $input = [
                'name'=>$request->name,
                'harga'=>$request->harga
            ];
            $data->fill($input);
            $data->save();

            DB::commit();
            return Redirect::route('admin.kategori')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update(Request $request,$id)
    {
        $rules = [
            'name' => 'required',
            'harga' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();

            $data = Kategori::where('id', $id);
            $input = [
                'name'=>$request->name,
                'harga'=>$request->harga
            ];
            $data->update($input);

            DB::commit();
            return Redirect::route('admin.kategori')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Kategori::where('id',$id)->delete();
            DB::commit();
            return Redirect::route('admin.kategori')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function datatable()
    {
        return $data = DataTables::of(Kategori::orderBy('created_at', 'desc')->get())
        ->make(true);
        $this->make($data);
    }

}
