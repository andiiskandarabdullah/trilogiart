<?php

namespace App\Http\Controllers\Desainer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Order;

class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('desainer.pemesanan.index');
    }

    public function updateStatus ($id)
    {
        try {
            DB::beginTransaction();

            Order::where('id', $id)->update([
                'status' => 2,
            ]);

            DB::commit();
            return Redirect::route('desainer.pemesanan')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function uploadDesain(Request $request, $id)
    {
			try {
					DB::beginTransaction();
					$imageUrl = $request->file('desain');
					if ($imageUrl == null) {
							$img =  '';
					} else {
							$imageName = time().'.'.$request->file('desain')->getClientOriginalExtension();
							$request->file('desain')->move(public_path('/assets/dist/images'), $imageName);
							$img = '/assets/dist/images/'.$imageName;
					}
					Order::where('id', $id)->update([
							'desain' => $img,
							'status' => 3,
					]);

					DB::commit();
					return Redirect::route('desainer.pemesanan')->with('success','Edit Success');
			} catch (\Throwable $th) {
					DB::rollback();
					throw $th;
			}
    }
    public function datatable()
    {
        return $data = DataTables::of(Order::with('kategori','customer')
        ->where('orders.desainer_id', auth()->user()->id)
        ->orderBy('created_at', 'desc')
        ->get())
        ->make(true);
        $this->make($data);
    }

		public function getDetail($id)
		{
			$pemesanan = Order::with('kategori','customer')->where('id', $id)->first();
			return view('desainer.pemesanan.show', compact('pemesanan'));
		}
}
