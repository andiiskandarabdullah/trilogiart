<?php

namespace App\Http\Controllers\Desainer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Galery;
use App\Models\Kategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class GaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('desainer.galeri.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $kategori = Kategori::get();
        return view('desainer.galeri.create', compact('kategori'));
    }

    public function edit(Request $request, $id)
    {
        $kategori = Kategori::get();
        $galery = Galery::where('id',$id)->first();
        return view('desainer.galeri.edit',['data'=>$galery, 'kategori'=>$kategori]);
    }

    public function store(Request $request)
    {
        $rules = [
            'kategori_id' => 'required',
            'caption' => 'required',
            'img' => 'required',
        ];
        $request->validate($rules);
        try {
            DB::beginTransaction();
            $imageUrl = $request->file('img');
            if ($imageUrl == null) {
                $img = $request->img;
            } else {
                $imageName = time().'.'.$request->file('img')->getClientOriginalExtension();
                $request->file('img')->move(public_path('/assets/dist/images'), $imageName);
                $img = '/assets/dist/images/'.$imageName;
            }
            $data = new Galery();

            $input = [
                'kategori_id' => $request->kategori_id,
                'desainer_id' => auth()->user()->id,
                'caption' => $request->caption,
                'img' => $img,
            ];
            $data->fill($input);
            $data->save();

            DB::commit();
            return Redirect::route('desainer.galeri')->with('success','Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update(Request $request,$id)
    {
        try {
            DB::beginTransaction();

            $data = Galery::where('id', $id)->first();
            $imageUrl = $request->file('img');
            if ($imageUrl == null) {
                $img =  $data->img;
            } else {
                $imageName = time().'.'.$request->file('img')->getClientOriginalExtension();
                $request->file('img')->move(public_path('/assets/dist/images'), $imageName);
                $img = '/assets/dist/images/'.$imageName;
            }
            Galery::where('id', $id)->update([
                'kategori_id' => $request->kategori_id,
                'caption' => $request->caption,
                'img' => $img,
            ]);

            DB::commit();
            return Redirect::route('desainer.galeri')->with('success','Edit Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Galery::where('id',$id)->delete();
            DB::commit();
            return Redirect::route('desainer.galeri')->with('success','Hapus Success');
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function datatable()
    {
        return $data = DataTables::of(Galery::with('kategori')
        ->where('desainer_id', auth()->user()->id)
        ->orderBy('created_at', 'desc')
        ->get())
        ->make(true);
        $this->make($data);
    }
}
